/**
# Reinitialization of the level-set function

Redistancing function with subcell correction, see the work of [Russo et al.,
1999](#russo_remark_2000). 

Let $\phi$ be a function close to a signed function that has been perturbed by
numerical diffusion. By iterating on this equation :
$$
\dfrac{\partial \phi}{\partial t} = sign(\phi^{0}) \left(1- \nabla \phi\right)
$$
we can correct or redistance $\phi$ to make it a signed function.



 */

struct LS_reinit {
  scalar dist;
  double dt;
  int it_max;
};


void LS_reinit(struct LS_reinit p){
  scalar dist = p.dist; 
  double dt = p.dt;     // standard timestep (0.5*Delta)
  int it_max = p.it_max;// maximum number of iteration (100)
  if(dt == 0) dt = 0.5 * L0/(1 << grid->maxdepth);

  if(it_max == 0)it_max = 100;

  vector gr_LS[];
  int i ;
  double eps = dt/100., eps2 = eps/2.;

  /**
We create `dist0[]` which will be a copy of the initial level-set function
before the iterations and `temp[]` which will be $\phi^{n}$ used for the
iterations.
*/
  scalar dist0[];
  foreach(){
    dist0[] = dist[] ;
  }
  boundary({dist0});

  /**
Iteration loop
*/
  for (i = 1; i<=it_max ; i++){
    double res=0.;
    scalar temp[];
    foreach(){
      temp[] = dist[] ;
    }
    boundary({temp});


    /**
First step, calculate $\Delta t$ which depends on the local position of the
0-level set. This time step is only used for the cells near the interface, we
tag them using `min_neighb` ($<0$ for interfacial cells).
*/
    double xCFL = 1.;
    foreach(reduction(min:xCFL)){
      double min_neighb = 1.;
      foreach_dimension(){
        min_neighb = min (min_neighb, temp[-1,0]*temp[]); 
        min_neighb = min (min_neighb, temp[ 1,0]*temp[]);
      }

      /**
Then we calculate the CFL for interfacial cells :
$$
CFL  = \min(\dfrac{\Delta x * \phi^0_i)}{\Delta \phi_i}
$$

Near the interface, *i.e.* for cells where:
$$
\phi^0_i\phi^0_{i+1} \leq 0 \text{ or } \phi^0_i\phi^0_{i-1} \leq 0
$$
the scheme must stay truly upwind, meaning that the movement of the 0
level-set of the function must be as small as possible. Therefore the upwind
numerical scheme is modified to :
$$\phi_i^{n+1} = \phi_i^n - \frac{\Delta t}{\Delta x} ( sign(\phi_i^0)
|\phi_i^n| - D_i)$$
*/
      if(min_neighb < 0.){
        double dist1= 0., dist2= 0.,dist3= 0.;
        foreach_dimension(){
          dist1 += pow((dist0[1,0]-dist0[-1,0])/2.,2.);
          dist2 += pow((dist0[1,0]-dist0[    ]),2.);
          dist3 += pow((dist0[   ]-dist0[-1,0]),2.);
        }
        double Dij = Delta*dist0[]/
        max(eps2,sqrt(max(dist1,max(dist2,dist3))));
        // stability condition near the interface is modified
        xCFL = min(xCFL,fabs(Dij)/(Delta));
      }
    }
    // if(xCFL < 0.1)fprintf(stderr, "%g\n", xCFL);
    foreach(reduction(max:res)){
      double delt =0.;
      //min_neighb : variable for detection if cell is near
      //             the zero of the level set function

      double min_neighb = 1.;
      foreach_dimension(){
        min_neighb = min (min_neighb, temp[-1,0]*temp[]);
        min_neighb = min (min_neighb, temp[ 1,0]*temp[]);
      }

      if(min_neighb < 0.){ // the cell contains the interface
        double dist1= 0., dist2= 0.,dist3= 0.;
        foreach_dimension(){
          dist1 += pow((dist0[1,0]-dist0[-1,0])/2.,2.);
          dist2 += pow((dist0[1,0]-dist0[    ]),2.);
          dist3 += pow((dist0[   ]-dist0[-1,0]),2.);
        }
        /**
Here, we set:
$$D_i = \Delta x * \frac{\phi_i^0}{\Delta \phi_i^0}$$

and:  
$$\Delta \phi_0^i = \max((\phi^0_{i-1}-\phi^0_{i+1})/2,\phi^0_{i-1}-\phi^0_{i},
\phi^0_{i}-\phi^0_{i+1})$$  
*/
        double Dij = Delta*dist0[]/
        max(eps2,sqrt(max(dist1,max(dist2,dist3))));
        delt = (sign2(dist0[])*fabs(temp[])-Dij)/Delta;
      }
      else{ 

        /**
Far from the interface, we use :
$$\phi_i^{n+1} = \phi_i^n - \Delta t * sign(\phi_i^0) G(\phi)_i$$
with:
$$
G(\phi_i) = \left\{ \begin{array}{ll}
max ( |a_+|, |b_-|) &, \text{ if } \phi_i^0 >0 \\
max ( |a_-|, |b_+|) &, \text{ if } \phi_i^0 <0 \\
\end{array}
\right.
$$
and:
$$
a= D_x^-\phi_i = (\phi_i -\phi_{i-1})/\Delta x\\
b= D_x^+\phi_i = (\phi_{i+1}- \phi_{i})/\Delta x
$$
which is a classical upwind scheme.
*/
        if(dist0[]>0){ // no interface in the cell
          foreach_dimension(){
            double a = max(0.,(temp[]    - temp[-1,0])/Delta);
            double b = min(0.,(temp[1,0] - temp[]    )/Delta);
            delt   += max(pow(a,2.),pow(b,2.));
          }
          delt = sign2(dist0[])*(sqrt(delt) - 1.);
        }
        else{
          foreach_dimension(){
            double a = min(0.,(temp[]    - temp[-1,0])/Delta);
            double b = max(0.,(temp[1,0] - temp[]    )/Delta);
            delt   += max(pow(a,2.),pow(b,2.));
          }
          delt = sign2(dist0[])*(sqrt(delt) - 1.);
        }
      }
      if(fabs(dist0[])>Delta/2.){
        dist[] -= 0.5*dt*delt;
      }
      else{
        dist[] -= xCFL*dt*delt;
      }
      if(fabs(delt)>=res) res = fabs(delt);
    }

    boundary({dist});
    restriction({dist});

    /**
Iterations are stopped when $L_1 = max(|\phi_i^{n+1}-\phi_i^n|) < eps$
*/
    if(res<eps){
      break;
    }
  }
}


/**
## References

~~~bib

@article{russo_remark_2000,
  title = {A remark on computing distance functions},
  volume = {163},
  number = {1},
  journal = {Journal of Computational Physics},
  author = {Russo, Giovanni and Smereka, Peter},
  year = {2000},
  pages = {51--67}
}

~~~
*/