/**
# Reconstruction of a velocity off an interface
*/  
struct LS_recons {
  scalar dist;
  double dt;
  int nb_cell_NB;
  double NB;
  scalar * LS_speed;
  int k_limit;
  double tolerance;
  int * err;
  int nb_iter;
  double overshoot;
};

void recons_speed(struct LS_recons p){
  scalar dist       = p.dist;
  double dt         = p.dt;
  int nb_cell_NB    = p.nb_cell_NB;
  double NB         = p.NB;
  scalar * LS_speed = p.LS_speed;
  int k_limit       = p.k_limit; // limitation activated by default
  double tolerance  = p.tolerance; // default tolerance is 1.e-8
  int * err         = p.err;
  int nb_iter       = p.nb_iter; // default is 50
  double overshoot  = p.overshoot; // default is 0, no overshoot tolerated
/**
The phase change field is only defined in interfacial cells, it must now be
reconstructed on the cells in the direct vicinity. We use here the method
described by [Peng et al., 1999](#peng_pde-based_1999) by solving the following
equation:

$$
\dfrac{\partial v_{pc}}{\partial t}  + S(\phi) \frac{\nabla \phi}{|\nabla \phi|}. \nabla v_{pc}= 0
$$

First, we calculate : 
$$ 
S(\phi) \frac{\nabla \phi}{|\nabla \phi|}
$$

using a centered approximation.
*/

  vector n_dist[], grad_dist[];

  *err = 1;

  if(tolerance == 0){
    tolerance = 1.e-8;
  }

  foreach(){

    double sum=1e-15;
    foreach_dimension(){
      grad_dist.x[] = min(dist[1,0]-dist[],min(dist[]-dist[-1,0],
        (dist[1,0]-dist[-1,0])/2.))/Delta;
      sum += grad_dist.x[]*grad_dist.x[];

    }
    foreach_dimension(){
      n_dist.x[] = grad_dist.x[]/(sqrt(sum)*sign2(dist[])+1.e-10);
    }
  }
  boundary((scalar *) {n_dist});

/**
Then, do the advection a few times to extend the velocity from the 
surface along the normal to the surface.

This second part is a bit tricky since our system has been initialized with the
velocity calculated on the face centroids $\left. u_{pc} \right|_{\Gamma}$ (see
Figure, in red). Thus, while we advect the velocity off the interface, we use an
iterative method to correct the cell-centered values upc (in blue) so that their
bilinear interpolation matches the correct interfacial boundary condition
$\left. u_{pc}\right|_{\Gamma}$.

![Initial face values (red) and cell-centered values (blue) in interfacial
cells](interp_abstract.svg)
 
*/ 

  int ii = 0;
  if(nb_iter == 0){
    nb_iter = 35;
  }
  int count = 0;
  foreach(reduction(+:count)){
    if(interfacial(point,cs)){
      count++;
    }
  }

/**
We iterate on the `scalar * LS_speed  = {v_pc.x,v_pc.y}` pointer rather than
using the foreach_dimension()
*/
  for (scalar f in LS_speed){
    double s2 = 0.;

  /**
  This first part avoids the creation of local extrema. We store the initial
  extrema in `min_f` and `max_f`.
  */
    scalar f_i[];
    scalar max_f[], min_f[];

    foreach(){
      f_i[] = f[];
      double temp = f[];
      foreach_dimension(){
        if(f[1,0] >temp)temp = f[1,0];
        if(f[-1,0]>temp)temp = f[-1,0];
      }
      max_f[] = temp;
      temp = f[];
      foreach_dimension(){
        if(f[1,0] <temp)temp = f[1,0];
        if(f[-1,0]<temp)temp = f[-1,0];
      }
      min_f[] = temp;
    }
    boundary({f_i,max_f,min_f});
  /**
  We introduce under-relaxation in our correction method to avoid the occurence of
  instabilities.

  First, we advect the velocity without modifying the velocity in interfacial
  cells.*/
    for (ii=1; ii<=(nb_iter*nb_cell_NB-1); ii++){
      scalar f2[];
      foreach(){
        f2[] = f[];
      }
      boundary({f2});
      restriction({f2});

      foreach(){
        if(fabs(dist[])<0.8*NB){
          if(!interfacial(point,cs)){
            foreach_dimension(){
              f[] -= dt *(max(n_dist.x[],0.)*(f2[   ]-f2[-1,0])/Delta
               +min(n_dist.x[],0.)*(f2[1,0]-f2[    ])/Delta);
            }
          }
        }
      }
      boundary({f});
      restriction({f});

      bool stop  = 0;


      double delt_err = 0.;
  /**
  Every `5*nb_cell_NB` we calculate the error between the bilinearly interpolated
  value from the cell-center field and modify the value in interfacial cells. */
      if( ii%(5*nb_cell_NB) == (5*nb_cell_NB-1) ){

        stop = 1;
        delt_err = 0.;

        foreach(){
          f2[] = f[];
        }
        boundary({f2});
        restriction({f2});
        foreach(reduction(+:delt_err)){
          if(interfacial(point, cs)){
            coord n       = facet_normal( point, cs ,fs) , p;
            normalize(&n);
            double alpha  = plane_alpha (cs[], n);
            line_length_center (n, alpha, &p);

            int Stencil[2];
            double coeff[4];

  /**
  The interpolation stencil is chosen according to the position of the face
  centroid.
  */
            InterpStencil(p, Stencil);

            coord p_interp = {p.x, p.y};

            double f_temp = mybilin( point , f2, Stencil, p_interp, coeff);

            int  select = 0;
            if (Stencil[0] == -1 && Stencil[1] == -1) select = 3;
            if (Stencil[0] ==  0 && Stencil[1] == -1) select = 2;
            if (Stencil[0] == -1 && Stencil[1] ==  0) select = 1;

            double error  = f_i[] - f_temp;
              // double temp1 = max_f[] + (max_f[] - min_f[])/(f_i[]+1.e-10);
            double temp1 = max_f[] ;
              // double temp2 = min_f[];
            double temp2 = min_f[];

  /**
  We only take $1/6^{th}$ of the error and use the `clamp()` function to avoid
  creating local extrema.
  */
            f[] = (k_limit)*(f[]+error/(3.*coeff[select]+1.e-16))
            + (1-k_limit)*(clamp(f[]+error/(6.*coeff[select]+1.e-16), 
              temp2+sign2(temp2)*overshoot*fabs(temp2), 
              temp1+sign2(temp1)*overshoot*fabs(temp1)));
  /**
  We then store in `f_temp2` the relative error between the interpolated value and
  the value previously calculated. 
  */
            double f_temp2 = f[Stencil[0],Stencil[1]]*coeff[0] + 
            f[Stencil[0]+1,Stencil[1]]*coeff[1] + 
            f[Stencil[0],Stencil[1]+1]*coeff[2] + 
            f[Stencil[0]+1,Stencil[1]+1]*coeff[3] ; 
            delt_err += pow((f_temp2-f_i[]),2.);
          }
        }
      }
      boundary({f});
      restriction({f});
      s2 = sqrt(delt_err)/count;
      if(stop){
        // fprintf(stderr, "it. %d DELT ERR %g S2 %g\n", ii/nb_cell_NB/5+1, 
          // delt_err, s2);
          if(s2 < tolerance){ // trouver un seuil variable d'arrêt
            // fprintf(stderr, "it. %d DELT ERR %g S2 %g\n", ii/nb_cell_NB/5+1, 
              // delt_err, s2);
          *err = 0;
          break;
        }
      }
    }
  }
}
/**
## References

~~~bib

@article{peng_pde-based_1999,
  title = {A {PDE}-Based Fast Local Level Set Method},
  volume = {155},
  issn = {0021-9991},
  url = {http://www.sciencedirect.com/science/article/pii/S0021999199963453},
  doi = {10.1006/jcph.1999.6345},
  pages = {410--438},
  number = {2},
  journaltitle = {Journal of Computational Physics},
  author = {Peng, Danping and Merriman, Barry and Osher, Stanley and Zhao, Hongkai and Kang, Myungjoo},
  urldate = {2019-09-09},
  date = {1999-11}
}
~~~
*/