/**
# Advection of a level-set function 

These functions are only redefinition of other functions (fluxes and advection) 
in order to ignore the presence of an embedded boundary.

## Timestep

We need to define a timestep on the mesh without taking into accound the
embedded boundary.
*/
double timestep_LS (const face vector u, double dtmax)
{
  static double previous = 0.;
  dtmax /= CFL;
  foreach_face(reduction(min:dtmax))
    if (u.x[] != 0.) {
      double dt = Delta/fabs(u.x[]);
      if (dt < dtmax) dtmax = dt;
    }
  dtmax *= CFL;
  if (dtmax > previous)
    dtmax = (previous + 0.1*dtmax)/1.1;
  previous = dtmax;
  return dtmax;
}

/**

## Fluxes

We redefine the fluxes for the level set advection and reconstruction of the
associated velocity field, they do not take into account the embedded boundaries
*/ 

void tracer_fluxes_LS (scalar f,
  face vector uf,
  face vector flux,
  double dt,
  (const) scalar src)
{

  /**
  We first compute the cell-centered gradient of *f* in a locally-allocated
  vector field. */

  vector g[];
  gradients ({f}, {g});

  /**
  For each face, the flux is composed of two parts... */

  foreach_face() {

    /**
    A normal component... (Note that we cheat a bit here, `un` should
    strictly be `dt*(uf.x[i] + uf.x[i+1])/((fm.x[] +
    fm.x[i+1])*Delta)` but this causes trouble with boundary
    conditions (when using narrow '1 ghost cell' stencils)). */

    double un = dt*uf.x[]/(Delta + SEPS), s = sign(un);
    int i = -(s + 1.)/2.;
    double f2 = f[i] + (src[] + src[-1])*dt/4. + s*(1. - s*un)*g.x[i]*Delta/2.;

    /**
    and tangential components... */


    double vn = (uf.y[i] + uf.y[i,1])/2.;
    double fyy = vn < 0. ? f[i,1] - f[i] : f[i] - f[i,-1];
    f2 -= dt*vn*fyy/(2.*Delta);


    flux.x[] = f2*uf.x[];
  }

  /**
  Boundary conditions ensure the consistency of fluxes across
  variable-resolution boundaries (on adaptive meshes). */

  boundary_flux ({flux});
}

/**
## Advection
*/

struct Advection_LS {
  scalar * level_set;
  face vector u;
  double dt;
  scalar * src; // optional
};


void advection_LS (struct Advection_LS p) 
{
  /**
  If *src* is not provided we set all the source terms to zero. */

  scalar * lsrc = p.src;
  if (!lsrc) {
    const scalar zero[] = 0.;
    for (scalar s in p.level_set)
      lsrc = list_append (lsrc, zero);
  }

  assert (list_len(p.level_set) == list_len(lsrc));
  scalar f, src;
  for (f,src in p.level_set,lsrc) {
    face vector flux[];
    tracer_fluxes_LS (f, p.u, flux, p.dt, src);
    foreach(){
      foreach_dimension(){
        f[] += p.dt*(flux.x[] - flux.x[1])/(Delta); 
      }
    }
  }
  boundary (p.level_set);

  if (!p.src)free (lsrc);
}
