/**
## My set of functions

A function to rescale normals so that they are unit vectors w.r.t. the 2-norm
(by default, the 1-norm is adopted for efficiency purposes).*/
#ifdef VOF
coord normal (Point point, scalar c) {
  coord n = mycs (point, c);
  double nn = 0.;
  foreach_dimension()
    nn += sq(n.x);
  nn = sqrt(nn);
  foreach_dimension()
    n.x /= nn;
  return n;
}


/**
A function to compute 2-norm normal in every cell. */

void compute_normal (scalar f, vector normal_vector) {
  foreach() {
    coord n = normal (point, f);
    foreach_dimension() 
      normal_vector.x[] = n.x;
  }
  boundary((scalar*){normal_vector});
}
#endif
/** 
#sign function
*/
double sign2 (double x)
{
  return(x > 0. ? 1. : x<0 ? -1. : 0.);
}

void InterpStencil (coord p, int Stencil[]){
  if(p.x<0){Stencil[0] = -1;} 
  else{Stencil[0] = 0;} 

  if(p.y<0) {Stencil[1] = -1;}
  else {Stencil[1] = 0;}
}

double capteur(Point point, scalar s){
  return s[];
}


double linearInterpolation(double x1, double f_x1, double x2, double f_x2, 
  double x)
{
  double result = (x - x1)/(x2-x1)*f_x2  + (x2-x)/(x2-x1)*f_x1;
  return result;
}

/**
Bilinear interpolation of a scalar s
*/
double mybilin (Point point, scalar s, int Stencil[], coord p_interp,
  double coeff[]){
  #if dimension == 2

  double dx = p_interp.x - Stencil[0];
  double dy = p_interp.y - Stencil[1];
  double dxdy = dx*dy;
  // f11
  coeff[0] = 1. - dx - dy + dxdy;
  // f21
  coeff[1] = dx - dxdy;
  // f12 
  coeff[2] = dy - dxdy;
  // f22
  coeff[3] = dxdy;

  return  s[Stencil[0],Stencil[1]]*coeff[0] + 
          s[Stencil[0]+1,Stencil[1]]*coeff[1] + 
          s[Stencil[0],Stencil[1]+1]*coeff[2] + 
          s[Stencil[0]+1,Stencil[1]+1]*coeff[3] ; 

  #endif
}

void cell2node(scalar cell_scalar, vertex scalar node_scalar){
  int Stencil[2] = {-1,-1};
  coord p_interp = {-0.5, -0.5};
  foreach_vertex(){
    double coeff[4];
    node_scalar[] = mybilin(point , cell_scalar, Stencil, p_interp, coeff);
  }
  boundary ({node_scalar});
  restriction({node_scalar});

}