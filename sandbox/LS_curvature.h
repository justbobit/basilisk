/**
#Curvature of a level_set function
*/

#include "alex_functions.h"
#include "fractions.h"

void curvature_LS(scalar LS, scalar curve_LS){

  vector gr_LS[];

  foreach(){
    double norm = 1.e-15;
    foreach_dimension(){
      double temp = (LS[1,0]-LS[-1,0])/(2.*Delta);
      gr_LS.x[] = temp;
      norm += sq(temp); 
    }
    norm = sqrt(norm);
    foreach_dimension(){
      gr_LS.x[] /= norm;
    }
  }
  boundary((scalar *){gr_LS});
  
  scalar phixx[], phixy[], phiyy[];

  foreach(){
    phixx[] = (gr_LS.x[1,0] - gr_LS.x[-1,0])/(2.*Delta); 
    phiyy[] = (gr_LS.y[0,1] - gr_LS.y[0,-1])/(2.*Delta); 
    phixy[] = ((gr_LS.y[1,0] - gr_LS.y[-1,0])/(2.*Delta) +  
     (gr_LS.x[0,1] - gr_LS.x[0,-1])/(2.*Delta))/2.;
  }

  boundary({phixx, phixy, phiyy});
  restriction({phixx, phixy, phiyy});

  scalar c1[];
  foreach(){
    c1[]= (sq(gr_LS.y[])*phixx[] 
      - 2.*gr_LS.x[]*gr_LS.y[]*phixy[] 
      + sq(gr_LS.x[])*phiyy[])/
    powf(1.e-15 + (sq(gr_LS.x[]) + sq(gr_LS.y[])),1.5);
  }
  boundary({c1});
  restriction({c1});

  foreach(){
    if(interfacial(point,cs)){
      coord n       = facet_normal( point, cs ,fs) , p;
      normalize(&n);
      double alpha  = plane_alpha (cs[], n);
      line_length_center (n, alpha, &p);

      int Stencil[2];
      double coeff[4];

/**
The interpolation stencil is chosen according to the position of the face
centroid.
*/
      InterpStencil(p, Stencil);

      coord p_interp = {p.x, p.y};

      curve_LS[] = -mybilin( point , c1, Stencil, p_interp, coeff);
    }
    else{
      curve_LS[] = nodata;
    }
  }
  boundary({curve_LS});
  restriction({curve_LS});

}
