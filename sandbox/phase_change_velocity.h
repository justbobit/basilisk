/**
# Phase change velocity calculation

The following set of functions modifies the phase change velocity field 
$\mathbf{v}_{pc} = v_{pc} \mathbf{n}_{2\rightarrow 1}$ in interfacial cells 
and sets it to :
$$
\mathbf{v}_{pc} = \frac{1}{L_H}\left(\lambda_{1}\left.\nabla tr_{1}\right|_
{\Gamma} - \lambda_{2}\left.\nabla tr_{2}\right|_{\Gamma}\right) 
$$
which is the Stefan condition, where $L_H$ is the latent heat, $tr_{1}$ and $tr_{2}$ 
are scalars defined in phase 1 and phase 2 respectively and 
$\lambda_i$ are the thermal diffusivities.

## Anisotropy of the Gibbs-Thomson relation

We define 2 functions to take into account the anisotropy in the change velocity
where we set the following condition :
$$
\epsilon = \overline{\epsilon} \left( 1. - \alpha \cos(n \theta +
\theta_0)\right)
$$
where $\theta$ is the angle between the interface and the x-axis, $\alpha =0.5$
and $\theta_0 = 0$ are hardcoded. This will be used in the Gibbs-Thomson
formulation.
*/

double fac1(double x, double y, int n){
  assert(dimension == 2);
  if(n > 1){ // prefactor for GT-formulation
    double theta = atan2 (y,x);
    return (1.-1.5*cos(n*theta));
    // return (1.-0.7*pow(cos(n*theta/2.),4));
  }
  else return 1.;
}

/**
For the gradient calculation on the interface, we need the temperature on the
interface $T_{\Gamma}$, which is defined either by :
$$
 T_{\Gamma} = T_{m} - \epsilon_{\kappa} * \kappa - \epsilon_{v} v_{pc} = T_m -
  \left(\overline{\epsilon_{\kappa}} * \kappa - \overline{\epsilon_{v}} 
  v_{pc}\right)(1-0.5 cos(n \theta))
$$
which is the classical Gibbs-Thomson equation.
*/

double Temp_GT(Point point, double epsK, double epsV, vector v_pc,
  scalar curve, face vector fs, scalar cs, int aniso){
  double pref = 1.;
  if(aniso > 1)pref = fac1(facet_normal( point, cs ,fs).x,
                              facet_normal(point, cs ,fs).y,aniso);
  return  (epsK*curve[] +  epsV*sqrt(v_pc.x[]*v_pc.x[]+v_pc.y[]*v_pc.y[]))*pref;
}

/**
## Velocity in interfacial cells

This function modifies the `vector v_pc[]` in interfacial cells using the Stefan
condition. Note that this procedure is not sufficient to obtain a proper phase
change velocity field used for a level-set, it must be complemented with a
reconstruction procedure (see [this page](LS_recons.h))*/

void phase_change_velocity_LS_embed (scalar cs, face vector fs, scalar tr,
 scalar tr2, vector v_pc, double L_H, double lambda[2], double epsK, 
 double epsV, int aniso) {

  scalar T_eq[];

  scalar curve[];
  curvature (cs,curve);
  boundary({curve});


/**
We store in `scalar T_eq[]` the temperature on the interface.
*/
#if Gibbs_Thomson

  foreach(){
    // here we suppose that Tm = 0
    T_eq[] = Temp_GT(point, epsK, epsV, v_pc, curve, fs, cs, aniso);
  }

#else
  foreach(){
    T_eq[] = 0;
  }
#endif

  boundary({T_eq});
  restriction({T_eq});

/**
To calculate the gradient $\left. \nabla tr \right|_{\Gamma}$, we use the
embed_gradient_face_x defined in embed that gives an accurate definition of the
gradients with embedded boundaries. */
  vector gtr[], gtr2[];
  boundary({tr});

  foreach(){
    foreach_dimension(){
      gtr.x[] = 0.;
    }
    if(interfacial(point, cs)){
      coord n       = facet_normal( point, cs ,fs) , p;
      normalize(&n);
      double alpha  = plane_alpha (cs[], n);
      line_length_center (n, alpha, &p);
      double c    = 0.;
      double temp = T_eq[];
      double grad = dirichlet_gradient(point, tr, cs , n, p, 
        temp, &c);

/**
For degenerate cases (stencil is too small), we add the term $tr[]*c$ to the 
gradient.
*/
      foreach_dimension(){
        gtr.x[] += grad*n.x+tr[]*c;
      }
    }
  }

  boundary((scalar*){gtr});

/**
Now we need to change the volume fraction and face fractions to calculate the
gradient in the other phase.
*/
  foreach(){
    cs[]      = 1.-cs[];
  }
  foreach_face()
  fs.x[]      = 1.-fs.x[];

  boundary({cs,fs});
  restriction({cs,fs});

  boundary({tr2});
  vector p_sauv[], n_sauv[];

  foreach(){
    foreach_dimension(){
      gtr2.x[] = 0.;
    }
    if(interfacial(point, cs)){
      coord n       = facet_normal( point, cs ,fs) , p;
      normalize(&n);
      double alpha  = plane_alpha (cs[], n);
      line_length_center (n, alpha, &p);
      double c=0.;
      double temp = T_eq[];
      double grad = dirichlet_gradient(point, tr2, cs , n, p, 
        temp, &c);
      foreach_dimension(){
        gtr2.x[] += grad*n.x+tr2[]*c;
      }
    }
  }
  boundary((scalar*){gtr2});
  foreach(){
    cs[]      = 1.-cs[];
  }
  foreach_face()
  fs.x[]      = 1.-fs.x[];
  boundary({cs,fs});
  restriction({cs,fs});

  /**
  With the the normal vector and the gradients of the tracers we can now 
  compute the phase change velocity $\mathbf{v}_{pc}$. */

  foreach_face(){
    v_pc.x[]  = 0.; 
  }

  foreach_face(){
    if(interfacial(point, cs)){
      v_pc.x[]     =  (lambda[1]*gtr2.x[] - lambda[0]*gtr.x[])/L_H;
    }
  }
  boundary((scalar *){v_pc});
}
