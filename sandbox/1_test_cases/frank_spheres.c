/**
#Frank's Spheres

This theory of this test case has been studied originally by [Frank](#frank1950radially).


We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_2 \nabla T_S)
  $$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The full algorithm is done on two iterations can be found on the mini_cell test
case.

*/

#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846


#include "embed.h"
#include "../double_embed-tree.h"

#include "../advection_A.h"
#include "diffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "../level_set.h"
#include "../LS_reinit.h"
#include "../../popinet/redistance.h"
#include "../LS_recons.h"
#include "../LS_advection.h"
#include "../phase_change_velocity.h"
#include "../LS_curvature.h"
#include "view.h"


#define T_eq          0.
#define TL_inf       -1./2
#define TS_inf        0.

#define tstart 0.

int MINLEVEL, MAXLEVEL; 
double H0;
double latent_heat;

#define DT_MAX  1.

#define T_eq         0.

scalar TL[], TS[], dist[];
scalar * tracers = {TL};
scalar * tracers2 = {TS};
scalar * level_set = {dist};

vector v_pc[];
scalar * LS_speed   = {v_pc.x,v_pc.y};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];

#if Gibbs_Thomson // parameters for the Gibbs-Thomson's equation
double  epsK = 0.0001, epsV = 0.000;
#else
double  epsK = 0.000, epsV = 0.000;
#endif
scalar curve[];


double lambda[2];

#define GT_aniso 0


int     nb_cell_NB =  1 << 3 ;  // number of cells for the NB
double  NB_width ;              // length of the NB

#include "basic_geom.h"
  
mgstats mg1,mg2;

TL[embed] = dirichlet(T_eq);
TS[embed] = dirichlet(T_eq);

TS[top]    = dirichlet(TL_inf); 
TS[bottom] = dirichlet(TS_inf); 
TS[left]   = dirichlet(TS_inf); 
TS[right]  = dirichlet(TS_inf); 


int j;
int k_loop = 0;


int main() {

  L0 = 16.;
  CFL = 0.5;
  origin (-0.5*L0, -0.5*L0);
  
  j = 1;
  for (j=1;j<=1;j++){

/**
Here we set up the parameters of our simulation. The latent heat $L_H$, the
initial position of the interface $h_0$ and the resolution of the grid.
*/
    latent_heat  = 1;
    MAXLEVEL = 9, MINLEVEL = 5 ;

    H0 = 0.5*L0; 
    N = 1 << MAXLEVEL;
    init_grid (N);
    run(); 
  }
}

event init(t=0){

  TOLERANCE = 1.e-7;
  DT = 0.0002;

  NB_width = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;
  double R_init = 1.56;
  coord center = {0.00,0.00};
  foreach(){
      dist[] = circle(x,y,center,R_init);
  }
  boundary ({dist});
  restriction({dist});

  fprintf(stderr, "CHECK\n" );
  redistance(dist);

  vertex scalar dist_n[];
  cell2node(dist,dist_n);

  fractions (dist_n, cs, fs);
  boundary({cs,fs});
  restriction({cs,fs});

  curvature(cs,curve);
  // curvature_LS(dist,curve);
  boundary({curve});

  foreach() {
    TL[] = TL_inf ;
    TS[] = 0.;
  }

  foreach_face(){
    v_pc.x[] = 0.;
  }

  boundary({TL,TS});
  restriction({TL,TS});
  int n =0;
  foreach(reduction(+:n)){
      n++;
    }
  fprintf(stderr, "CELLS INIT%d\n", n);
}


event properties(i++){
  foreach_face()
  muv.x[] = lambda[i%2]*fs.x[];
  boundary((scalar *) {muv});
}

event tracer_diffusion(i++){
  int kk;
  mgstats mg1;
  for (kk=1;kk<=7;kk++){
    if(i%2==0){
      boundary({TL});
      mg1 = diffusion(TL, dt, D = muv , theta = cs);
    }
    else{
      boundary({TS});
      mg1 = diffusion(TS, dt, D = muv, theta = cs);
    }
    if((mg1.resa > TOLERANCE) ) {
/**
If the calculation crashes (it often does if the Poisson solver does not
converge) we save the last state of the variables
*/
      scalar ffsx[], ffsy[];
      foreach(){
        ffsx[] = fs.x[];
        ffsy[] = fs.y[];
      }
      boundary({ffsx,ffsy});
      dump();
      fprintf(stderr, "%d\n", i);
      exit(1);
    }
  }
}


/**
This event is the core the of the hybrid level-set/embedded boundary.
*/
event LS_advection(i++,last){
  if(i%2 ==1 && i > 20){

    double L_H       = latent_heat;  

    scalar cs0[];

/**
We need to change cs to 1-cs for all the level-set functions to work.
*/
    foreach(){
      cs0[]   = cs[];
      cs[]    = 1.-cs[];
    }
    foreach_face(){
      fs.x[]  = 1.-fs.x[];
    }

    boundary({cs,fs,cs0});
    restriction({cs,fs});
/**
First, we calculate the velocity on the face centroid
*/
    int aniso = 1;
    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, L_H, 
      lambda,epsK, epsV, aniso);
    double deltat  = 0.45*L0 / (1 << MAXLEVEL);  // Delta
    int err = 0;
    int k_limit = 0;


    stats s2 = statsf(v_pc.y);
    fprintf(stderr, "%g %g\n", s2.max, s2.min);
/**
We copy this value in v_pc_r.
*/
    vector v_pc_r[];
    foreach(){
      foreach_dimension(){
        if(interfacial(point, cs))v_pc_r.x[] = v_pc.x[];
        else v_pc_r.x[] = 0.;
      }
    }
    boundary((scalar * ){v_pc_r});
    restriction((scalar * ){v_pc_r});

    scalar * speed_recons  = {v_pc_r.x,v_pc_r.y};

/**
We reconstruct a cell-centered v_pc field in the vicinity of the interface where
v_pc_r is the solution to the bilinear interpolation of v_pc on face centroids.
*/
    recons_speed(dist, deltat, nb_cell_NB, NB_width, speed_recons,
     k_limit, tolerance = 2.e-7, &err, nb_iter = 50, overshoot = 0.3);

    double dt_LS = timestep_LS (v_pc_r, DT);

/**
We do a cell-center to face change. Because we use embedded boundaries, we
cannot use the face_value routine.
*/
    face vector v_pc_f[];
    foreach_face(){
      v_pc_f.x[] = (v_pc_r.x[-1,0]+v_pc_r.x[])/2.;
    }
    boundary ((scalar *){v_pc_f});

    advection_LS (level_set, v_pc_f, dt_LS);
    
    boundary ({dist});
    restriction({dist});

/**
After the advection, we need to redistance the level-set function.
*/
    redistance(dist);

    scalar dist_n[];
    cell2node(dist,dist_n);
    fractions (dist_n, cs, fs);
    
    boundary({cs,fs});
    restriction({cs,fs});
    curvature(cs,curve);
    // curvature_LS(dist,curve);
    boundary({curve});

/**
We revert cs back to its original state.
*/
    foreach(){
      cs[]      = 1.-cs[];
    }
    foreach_face(){
      fs.x[]      = 1.-fs.x[];
    }
    boundary({cs,fs});
    restriction({cs,fs});

/**
Sometimes, when a new cell becomes an interfacial cell, the Poisson solver has
trouble converging, therefore we iterate a bit more when it occurs.
*/
    k_loop = 0;
    foreach(){
      if( (cs0[] != 1. && cs[] ==1.) || (cs0[] == 0. && cs[] !=0.))k_loop = 1;
    }
  }
}


#if DOUBLE_EMBED
event double_calculation(i++,last){
// modify the fs , cs, copy the outer fields in the partially covered cells

  foreach(){
    cs[]      = 1.-cs[];
  }
  foreach_face(){
    fs.x[]      = 1.-fs.x[];
  }

  boundary({cs,fs});
  restriction({cs,fs});
}
#endif

event movies ( i++,last;t<1.)
{
  if(i%2 == 1 ) {
    boundary({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});

    view (fov = 18.7687,width = 800, height = 800);

    draw_vof("cs");
    squares("visu");
    save("visu.mp4");

    draw_vof("cs");
    squares("v_pc.y");
    save ("v_pcy.mp4");

    // draw_vof("cs");
    // squares("dist", min =-NB_width, max = NB_width);
    // save ("dist.mp4");

      
    if(i%10==1) {
      output_facets (cs, stdout);
    }
  }
}

#if 1
event adapt (i++, last) {
  if(i%2 == 1 ){

    foreach_cell(){
      cs2[] = 1.-cs[];
    }
    foreach_face(){
        fs2.x[]      = 1.-fs.x[];
    }

    boundary({cs,cs2,fs,fs2});
    fractions_cleanup(cs,fs,smin = 1.e-14);
    fractions_cleanup(cs2,fs2,smin = 1.e-14);
    restriction({cs,cs2,fs,fs2});
    int n=0;
    // stats s2 = statsf(curve);
    // fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
    boundary({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});

    adapt_wavelet ({cs,curve,visu},
      (double[]){1.e-2,1.e-2,1.e-2},MAXLEVEL, MINLEVEL);
    foreach(reduction(+:n)){
      n++;
    }
    curvature(cs,curve);
    // curvature_LS(dist,curve);
    boundary({curve});
    int aniso = 1;
    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, latent_heat, 
      lambda,epsK, epsV, aniso);
    fprintf(stderr, "##nb cells %d %g\n", n,t);
  }
}
#endif

/**
![Animation of temperature field](frank_spheres/visu.mp4)(loop)


~~~gnuplot Evolution of the interface
set size ratio -1
plot 'out' w l t ''
~~~

~~~bib

@article{frank1950radially,
  title={Radially symmetric phase growth controlled by diffusion},
  author={Frank, Frederick Charles},
  journal={Proceedings of the Royal Society of London. Series A. Mathematical and Physical Sciences},
  volume={201},
  number={1067},
  pages={586--599},
  year={1950},
  publisher={The Royal Society London}
}
~~~
*/
