import numpy as np
import matplotlib.pyplot as plt
import math

def devlim ( x , n):
    y = np.log(x)
    for i in range(1,n):
        y = y + (-x)**i/(i*math.factorial(i))
    return y;

def Fd (x, n):
    return(devlim(x**2/4.,n));

x = 10
ordre_dev = 40

#Frank Spheres parameters from Almgren (92)

u_inf   = -0.5 # undercooling -0.5
S       = 1.56
t_init  = 1
t_final = 2
R_init  = 1.56
R_final = 2.21


r = np.linspace(R_init,2*R_init,100)
s = r/(t_init)**(1./2)
Fd_S = Fd(S,40)
# print(Fd_S)

u_s = -0.5 * ( 1. - Fd(s,50)/Fd_S)
# plt.plot(s,u_s)
r = np.linspace(R_final,2*R_final,100)
s2 = r/(t_final)**(1./2)
Fd_S = Fd(S,40)
u_s2 = -0.5 * ( 1. - Fd(s,50)/Fd_S)
u_s = u_s - u_s2
s = s - s2
print(s)
# plt.plot(s,u_s)
# plt.savefig('u_init.png')