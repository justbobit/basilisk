/**
# Stokes flow through a complex porous medium

The medium is periodic and described using embedded boundaries. 

This tests mainly the robustness of the representation of embedded
boundaries and the convergence of the viscous and Poisson
solvers. */

#include "embed.h"
#include "tracer.h"
#include "diffusion.h"
#include "view.h"

/**
We will vary the maximum level of refinement, starting from 5. */

int maxlevel = 9;

/**
The porous medium is defined by the union of a random collection of
disks. The number of disks can be varied to vary the porosity. */

void porous (scalar cs, face vector fs)
{
  int ns = 900;
  double xc[ns], yc[ns], R[ns];
  srand (0);
  for (int i = 0; i < ns; i++)
    xc[i] = 0.5*noise(), yc[i] = 0.5*noise(), R[i] = 0.01 + 0.02*fabs(noise());

  /**
  Once we have defined the random centers and radii, we can compute
  the levelset function $\phi$ representing the embedded boundary. */
  
  vertex scalar phi[];
  foreach_vertex() {
    phi[] = HUGE;

    /**
    Since the medium is periodic, we need to take into account all
    the disk images using periodic symmetries. */
      
    for (double xp = -L0; xp <= L0; xp += L0)
      for (double yp = -L0; yp <= L0; yp += L0)
	for (int i = 0; i < ns; i++)
	  phi[] = intersection (phi[], (sq(x + xp - xc[i]) +
					sq(y + yp - yc[i]) - sq(R[i])));
    phi[] = -phi[];
  }
  boundary ({phi});

  fractions (phi, cs, fs);
  fractions_cleanup (cs, fs);
}


/**
Lets study the temperature !
*/
scalar T[];
scalar * tracers = {T};

double gnu = 0.0001;

face vector muc[];

/**
The domain is the periodic unit square centered on the origin. */

int main()
{
  origin (-0.5, -0.5);
  periodic (right);
  

  /**
  We turn off the advection term. The choice of the maximum timestep
  and of the tolerance on the Poisson and viscous solves is not
  trivial. This was adjusted by trial and error to minimize (possibly)
  splitting errors and optimize convergence speed. */
  
  // stokes = true;
  DT = 2e-4;

#if 1
  TOLERANCE = HUGE;
  NITERMIN = 2;
#else
  TOLERANCE = 1e-3;
  NITERMIN = 2;
#endif
  N = 1 << maxlevel;
  run();
}

scalar un[];

event init (t = 0) {

  /**
  We define the porous embedded geometry. */

  porous (cs, fs);
  
  /**
  The boundary condition is zero velocity on the embedded boundary. */

  T[embed]         = neumann(-0.001);
  T[top]           = dirichlet(-20.);
  T[bottom]        = neumann(-0.001);
  
  /**
  We initialize the reference velocity. */
  
  foreach(){
    T[]  = 0.;
    un[] = 0.;
  }
}

event properties (i++) {
  foreach_face()
    muc.x[] = fm.x[]*gnu;
  boundary ((scalar*){muc});
}

event Diffusion (i++)
{
  mgstats mg1;

  mg1 = diffusion (T, dt, muc ,theta = cm);
  boundary(all);
  if(mg1.resa > TOLERANCE) {
    dump();
    exit(1);
  }
}

/**
We check for a stationary solution. */

event logfile (i++; i<=500)
{
  double avg = normf(T).avg, du = change (T, un)/(avg + SEPS);
  foreach(){
    un[] = T[];
  }
  boundary({un});

  fprintf (ferr, "%d %d %.3g \n", maxlevel, i, du) ;

  /**
  If the relative change of the velocity is small enough we stop this
  simulation. */
  
  if (i > 0 && (du < 1.)) {

    /**
    We output fields and dump the simulation. */
    
    view (fov = 19.3677);
  
    draw_vof ("cs", "fs", filled = -1, fc = {1,1,1});
    squares ("level");
    sprintf (name, "level-%d.png", maxlevel);
    save (name);

    draw_vof ("cs", "fs", filled = -1, fc = {1,1,1});
    squares ("T", linear = false, spread = -1);
    sprintf (name, "T-%d.png", maxlevel);
    save (name);

    sprintf (name, "dump-%d", maxlevel);
    dump (name);

    /**
    We stop at level 10. */
    
    if (maxlevel == 9)
      return 1; /* stop */

    /**
    We refine the converged solution to get the initial guess for the
    finer level. We also reset the embedded fractions to avoid
    interpolation errors on the geometry. */
    
    maxlevel++;
#if 0
    refine (level < maxlevel && cs[] > 0. && cs[] < 1.);
#else
    adapt_wavelet ({cs,T}, (double[]){1e-2,1e-6}, maxlevel);
#endif
    porous (cs, fs);
    boundary (all); // this is necessary since BCs depend on embedded fractions
  }
}


event movie(i++){
  if(maxlevel == 9){
    squares ("T", linear = true, spread = 8);
    save ("nu.mp4");
  }
}

/**
![Norm of the velocity field.](porous/nu-10.png)

![Pressure field.](porous/p-10.png)

![Adapted mesh, 10 levels of refinement.](porous/level-10.png)

~~~gnuplot Permeability as a function of resolution
set xlabel 'Level'
set grid
set ytics format '%.1e'
set logscale y
plot 'out' w lp t ''
~~~

~~~gnuplot Convergence history
set xlabel 'Iterations'
set logscale y
set ytics format '%.0e'
set yrange [1e-10:]
plot '../porous.ref' u 2:9 w l t '', '' u 2:10 w l t '', \
    '' u 2:11 w l t '', '' u 2:12 w l t '', '' u 2:13 w l t '', \
    'log' u 2:9 w p t 'du', '' u 2:10 w p t 'resp', \
    '' u 2:11 w p t 'resu', '' u 2:12 w p t 'u.x.sum', '' u 2:13 w p t 'p.max'
~~~

## See also

* [Stokes flow past a periodic array of cylinders](cylinders.c)
* [Stokes flow through a complex 3D porous medium](/src/examples/porous3D.c)
*/
