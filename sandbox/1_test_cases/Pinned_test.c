/**
 EVAPORATION OF A SESSILE DROPLET
 */

#include "navier-stokes/centered.h"
#include "contact.h"
#include "vof.h"
#include "two-phase.h"
#include "tracer.h"
#include "tension.h"
#include "../../qmagdelaine/phase_change/elementary_body.h"
#include "view.h"

//------------------------------------------------
// Simulation parameters
//------------------------------------------------

//Geometrical and dimensionless physical parameters

#define R0 2.5 //Radius of the sphere to form a spherical cap
#define GAMMA 1. //Surface tension
#define vcs 1. //Vapor at the interface
#define cinf 0.2 //Vapor at infinity
#define vapor_peclet 1e-1 //Peclet number to compare diffusion and convection

/**Dimensionless numbers
 RHOR 1.2e-3 //Densities ratio
 MUR 1.8e-2 //Dynamic viscosities ratio
 LAPLACE 2.16e5 //Laplace number
 SCHMIDT 0.53 //Schmidt number
 Pe 1e-3 //Peclet number
 S 0.2 //Vapor concentration ratio cinf/vcs
 */

#define D_V 1. //Diffusion coefficient of water vapor in air

//Numerical parameters

#define L 5. //Size of the box
#define MIN_LEVEL 6 //Mesh levels
#define LEVEL 7
#define MAX_LEVEL 8
#define dR_refine (2.*L0/(1 << LEVEL))
#define F_ERR 1e-10
#define DT_MAX 10.0
#define dirichlet_time_factor 10.

//------------------------------------------------
// Tracers definition
//------------------------------------------------

/** We have only have one tracer for now, the vapor in the gas phase.*/

scalar vapor[];
scalar * tracers = {vapor};

//------------------------------------------------
// Boundary conditions
//------------------------------------------------

/**By default, symmetrical conditions are applied on the left boundary.*/

//conditions for the vapor field
vapor[right] = dirichlet(cinf);
vapor[top]   = dirichlet(cinf);

//permeability of our domain
u.n[top] = neumann(0.);
u.n[right] = neumann(0.);

//non slipery conditions on the substrat
u.t[bottom] = dirichlet(0.);
uf.t[bottom] = dirichlet(0.);

//impermeability conditions on the substrat
u.n[bottom] = dirichlet(0.);
uf.n[bottom] = dirichlet(0.);

//------------------------------------------------
//Contact angle
//------------------------------------------------

/** To set the contact angle, we allocate a height function h and set 
 the contact angle boundary condition on its tangential component. */

vector h[];
double thetaR = 70;
double thetaA = 110;
double K = 0.01;
double my_CL_speed(Point point, double thetaA, double thetaR, double K,
  vector u, scalar f){
  coord n = normal(point,f);
  double theta = atan2(n.y,n.x);
  if(theta > thetaA) return K*(theta - thetaA);
  if(theta < thetaA) return K*(theta - thetaR);
  return 0. ;
}

h.t[bottom] = interfacial (point, f) ? ( (u.x[]*normal(point,f).x < 0) ? contact_angle(thetaR*pi/180) : contact_angle(thetaA*pi/180)): contact_angle((thetaR+thetaA)*pi/360.);
// uf.t[bottom] = interfacial (point, f) ? my_CL_speed(point, thetaA,thetaR, K, u ,f ) : 1.;
uf.t[bottom] = dirichlet(0.);


//------------------------------------------------
//Simulation
//------------------------------------------------

int main()
{
  size (L);
  origin (0.,0.);
  N = 1 << LEVEL;
  init_grid (N);
  DT = DT_MAX;
  
  //Densities : rho1 for water and rho2 for air
  rho1 = 10.;
  rho2 = 1;
  
  //Dynamic viscosities : mu1 for water and mu2 for air
  mu1 = 100.;
  mu2 = 1.;
  
  /** We must associate the height function field with the VOF tracer, so
   that it is used by the relevant functions (curvature calculation in
   particular). */
  f.height = h;
  
  //Surface tension coefficient
  f.sigma = GAMMA;
  
  run();
}


//------------------------------------------------
//Initialization
//------------------------------------------------

// The initial drop is a 2D spherical cap. 
#define circle(x, y, R0) (sq(R0) - sq(x) - sq(y))

event init (t = 0)
{ 
#if 0
  refine (level < MAX_LEVEL && circle(x, y, (R0 - dR_refine)) < 0.
      && circle(x, y, (R0 + dR_refine)) > 0.);
#endif
  fraction (f, circle(x,y,R0)); //fraction defines the intial shape of the drop
  foreach() {
    u.x[] = 0.; // velocity centered field is set to zero
    vapor[] = f[]*vcs + (1. - f[])*cinf; //initialization of the vector field
  }
  foreach_face()
  uf.x[] = 0.; // velocity face field is set to zero
  boundary({vapor, u, uf});
}

//------------------------------------------------
//Evaporation velocity calculation
//------------------------------------------------

/**The velocity due to evaporation is computed in the *stability()* event to take
 into account this velocity in the CFL condition. Before to modify $\mathbf{u}_f$
 we save it in another face vector field, in order to recover it just after the
 advection of the interface. */

face vector uf_save[];
face vector ev[]; //Face velocity due to evaporation

ev.t[bottom] = dirichlet(0.);

event stability (i++) {
  
  vapor.D = D_V;
  vapor.peclet = vapor_peclet;
  vapor.inverse = true;
  phase_change_velocity (f, vapor, ev);
  
  foreach_face() {
    uf_save.x[] = uf.x[];
    uf.x[] += ev.x[];
    }
  boundary((scalar*){uf});
}

//------------------------------------------------
//Advection of the tracer
//------------------------------------------------

/** After the *vof()* event, the evaporation velocity has to be set back to the
 *real* velocity. The evaporation velocity is not a flow velocity but just a
 deplacement of the interface. */

event tracer_advection (i++) {
  foreach_face()
    uf.x[] = uf_save.x[];
  boundary((scalar*){uf});
}

//------------------------------------------------
//Diffusion of the vapor concentration field
//------------------------------------------------

/**The concentration field diffuses at each timestep. We need for that the maximal
 level in the simulation. */

event tracer_diffusion(i++) {
  
#if TREE
  int max_level = MAX_LEVEL;
#else
  int max_level = LEVEL;
#endif
  
  vapor.D = D_V;
  vapor.tr_eq = vcs;
  vapor.inverse = true;
  dirichlet_diffusion (vapor, f, max_level, dt, dirichlet_time_factor);
}

event movie(t+=0.05;t<10){
  stats s = statsf (u.x);
  fprintf(stderr, "%d %g %g %g\n", i,t, s.min, s.max);

  scalar omega[];
  vorticity (u, omega);
  scalar psi[];
  psi[bottom] = dirichlet (0.);
  psi[top] = dirichlet (0.);
  psi[left] = dirichlet (0.);
  psi[right] = dirichlet (0.);
  poisson (psi, omega, tolerance = 1e-2);
  boundary ((scalar *) {psi});

  vertex scalar stream[];
  foreach_vertex()
    stream[] = (psi[0,-1] + psi[-1,-1] + psi[] + psi[-1])/4.;



  view (fov = 19.8242,  tx = -0.0174561, ty = -0.491339, 
    width = 1200, height = 600);
  draw_vof("f");
  squares("vapor");
  isoline ("stream", n = 20);
  mirror (n = {1, 0, 0}, alpha = 0.) {
    draw_vof("f");
    squares("vapor");
  }
  save("vapor.mp4");
}

event output_interf(t+= 0.5)
  output_facets(f,stdout);


/**
~~~gnuplot Equilibrium shapes for $15^\circ \leq \theta \leq 165^\circ$
set size ratio -1
unset key
unset xtics
unset ytics
unset border
plot 'out' w l, '' u (-$1):2 w l lt 1, 0 lt -1
~~~

*/