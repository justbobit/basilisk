/**
#LS_reinit() test case

This case is extracted from [Russo et al.,1999](#russo_remark_2000) we
initialize a perturbed distance field, where the zero level-set is an ellipse of
the form:
$$
\phi (x,y,0) = f(x,y) \times g(x,y)
$$
where the perturbation is :
$$
f(x,y) = \epsilon  + (x - x_0)^2 +(y - y_0)^2
$$
and the ellipse is :
$$
g(x,y) = \left( \sqrt{\frac{x^2}{A^2}+\frac{y^2}{B^2}} -R \right)
$$
with $A=2$, $B=1$, $R = 1$ , $x_0 = 3.5$, $y_0 = 2.$.

We want to recover a perfect distance field, \textit{i.e.} remove the initial
perturbation.

*/

#include "../../popinet/distance_point_ellipse.h"
#include "../../popinet/redistance.h"
#include "../alex_functions.h"
#include "../LS_reinit.h"
#include "basic_geom.h"
#include "view.h"

double perturb (double x, double y, double eps, coord center){
  return eps + sq(x - center.x) + sq(y - center.y);
}

void draw_isolines(scalar s, double smin, double smax, int niso, int w){
  vertex scalar vdist[];
  foreach_vertex()
  vdist[] = interpolate (s, x , y);
  boundary ({vdist});
  for (double sval = smin ; sval <= smax; sval += (smax-smin)/niso){
    isoline ("vdist", sval, lw = w);
  }
}


#define Pi 3.141592653589793
double eval_err_LS(double A, double B, int nb_point, scalar s){

/**
We will evaluate the flux on the theoretical 0-level-set of our
function with $N_\Sigma$ point $\mathbf{x_p}=(x_p, y_p)$ exactly on the
interface and defined such that:
$$
x_p = A \cos(\frac{2\pi p}{N_\Sigma}) \text{\hspace{1cm}} y_p = B \sin(\frac{2\pi
p}{N_\Sigma}) 
$$
Therefore, we want this function to calculate :
$$
E_\Sigma = \int_\Sigma |\phi(\mathbf(x),t)|ds
$$
that we discretize into:
$$
E_\Sigma = \frac{1}{2}\sum_{p=1}^{N_\Sigma}\left( |\tilde\phi(\mathbf
{x_p},t)| + |\tilde\phi(\mathbf{x_{p+1}},t)|\right) \left| \mathbf{x_{p+1}} - \mathbf{x_{p}} \right|
$$
Where $\tilde\cdot$ is the bilinear interpolation of $\phi$ at the desired
position, ideally it should be at least one order above the desired order of
accuracy of our method. In our case, this would require a bicubic interpolation
or a hermitian one.

*/
  double sum = 0.;
  for(int i = 1; i<=nb_point; i++){
// 1rst point
    coord pLS = {A*cos(2.*Pi*i/nb_point), B*sin(2.*Pi*i/nb_point)};
    Point p = locate (pLS.x, pLS.y);
    int n = 1 << p.level;
    coord ccenter;
    ccenter.x = X0 + (p.i - 1) * L0/n;
    ccenter.y = X0 + (p.i - 1) * L0/n;

    int Stencil[2];
    double coeff[4];
    coord p_interp = {pLS.x - ccenter.x, pLS.y - ccenter.y};
    InterpStencil(p_interp, Stencil);

    double temp = fabs(mybilin(p, s, Stencil, p_interp, coeff));

    coord pLS2 = {A*cos(2.*Pi*(i+1)/nb_point), B*sin(2.*Pi*(i+1)/nb_point)};
    p = locate (pLS2.x, pLS2.y);
    n = 1 << p.level;
    ccenter.x = X0 + (p.i - 1) * L0/n;
    ccenter.y = X0 + (p.i - 1) * L0/n;
    p_interp.x = pLS2.x - ccenter.x;
    p_interp.y = pLS2.y - ccenter.y;
    InterpStencil(p_interp, Stencil);
    double temp2 = fabs(mybilin(p, s, Stencil, p_interp, coeff));

    sum += (temp + temp2)*sqrt( sq(pLS.x-pLS2.x) + sq(pLS2.y - pLS2.y))/2.;
  }
  return sum;
}


scalar dist[];
scalar * level_set = {dist};

int main() {
  origin (-5., -5.);
  L0 = 10;
  double A = 4., B = 2.;

  int MAXLEVEL;
  for (MAXLEVEL = 6; MAXLEVEL <=8; MAXLEVEL++){
    init_grid (1 << MAXLEVEL);

    coord center_perturb = {3.5,2.};
    foreach(){
      double a,b;
      dist[] = DistancePointEllipse(4.,2.,x,y,&a, &b)*
      perturb(x,y, 0.1, center_perturb);
    }
    boundary({dist});
    view (fov = 18.8026);
    if(MAXLEVEL == 8){
      squares ("dist", map = cool_warm, min = -2, max = 2);
      draw_isolines(dist, -2., 2., 20, 1);
      save("dist_init.png");
    }

    int j = MAXLEVEL;
    scalar err[];

    redistance(dist);

    foreach(){
      double a,b;
      double dist_theo  = DistancePointEllipse(4.,2.,x,y,&a, &b);
      err[] = fabs(dist[]-dist_theo);
    }
    boundary({err}); 
    stats s = statsf(err);
    double dt_adim = 1./(1 << MAXLEVEL);
  
    double err2 = eval_err_LS(A, B, 300, dist);
    fprintf(stderr, "%d %g %g\n",1<<j, s.sum*sq(dt_adim), 
      err2*sq(dt_adim));  
    if(MAXLEVEL == 8){
      draw_isolines(dist, -2., 2., 20, 1);
      squares ("dist", map = cool_warm, min = -2, max = 2);
      save("dist_final.png");
    }
  }
}


/**
We show here the initial and final level-set for the same isovalues.

![Initial level-set](reinit_redistance/dist_init.png) 
![Final level-set](reinit_redistance/dist_final.png)


We plot the results after our redistancing function `LS_reinit()` several times
and study the convergence of the error on the signed distance function :

$$
L_1 = \sum{|\phi_{i,j} - g(x_i,y_i)|}(\Delta x)^2
$$

~~~gnuplot L1-error
set logscale
set xlabel "number of cells (2^n)"
set ylabel "L_1-norm"
f(x) = a + b*x
fit f(x) 'log' u (log($1)):(log($2)) via a,b
ftitle(a,b) = sprintf("%.3f-%4.2f*x", a, -b)
set xrange [32:512]
set xtics 32,2,512
plot 'log' u 1:2 t 'L_1-error', 'ref' u 1:2 t 'ref', exp(f(log(x))) t ftitle\
(a,b)
~~~

~~~gnuplot Error on $\phi_0$
set logscale
set grid
set xlabel "number of cells (2^n)"
set ylabel "Error on \phi_0"
plot 'log' u 1:3 t 'Error on \phi_0', 'ref' u 1:3 t 'ref'
~~~

## References

~~~bib

@article{russo_remark_2000,
  title = {A remark on computing distance functions},
  volume = {163},
  number = {1},
  journal = {Journal of Computational Physics},
  author = {Russo, Giovanni and Smereka, Peter},
  year = {2000},
  pages = {51--67}
}

~~~
*/