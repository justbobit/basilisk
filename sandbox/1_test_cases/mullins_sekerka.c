/**
#Mullins Sekerka instability

This theory of this test case has been studied originally by [Mullins and
Sekerka](#Mullins1964). Then it was used as a validation test case for several
numerical method (see the work of [Almgren](#Almgren1993) and [Chen et al.](#chen_simple_1997))

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_2 \nabla T_S)
  $$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The full algorithm is done on two iterations can be found on the mini_cell test
case.

*/

#define DOUBLE_EMBED 1
#define LevelSet     1
#define Gibbs_Thomson 0
#define Pi 3.14159265358979323846

#define CURVE_LS 1

#include "embed.h"
#include "../double_embed-tree.h"

#include "../advection_A.h"
#include "diffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "../level_set.h"
#include "../LS_reinit.h"
#include "../../popinet/redistance.h"
#include "../LS_recons.h"
#include "../LS_advection.h"
#include "../phase_change_velocity.h"
#include "../LS_curvature.h"
#include "view.h"


#define T_eq          0.
#define TL_inf       -1.
#define TS_inf        0.

#define tstart 0.

int MINLEVEL, MAXLEVEL; 
double H0;
double latent_heat;

#define DT_MAX  1.

#define T_eq         0.


#define plane(x, y, n) (x + 0.05*cos(2.*n*Pi*y))

scalar TL[], TS[], dist[];
scalar * tracers = {TL};
scalar * tracers2 = {TS};
scalar * level_set = {dist};

vector v_pc[];
scalar * LS_speed   = {v_pc.x,v_pc.y};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];

#if Gibbs_Thomson // parameters for the Gibbs-Thomson's equation
double  epsK = 0.0001, epsV = 0.000;
#else
double  epsK = 0.000, epsV = 0.000;
#endif
scalar curve[];



double lambda[2];

#define GT_aniso 0
#if GT_aniso
int aniso = 6;
#else
int aniso = 1;
#endif


int     nb_cell_NB =  1 << 3 ;  // number of cells for the NB
double  NB_width ;              // length of the NB


double s_clean = 1.e-10; // used for fraction cleaning

  
mgstats mg1,mg2;


TL[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, v_pc, curve, fs, cs, aniso));
TS[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, v_pc, curve, fs, cs, aniso));

TL[right]  = dirichlet(TL_inf); 
TS[left]   = dirichlet(TS_inf); 
TS[right]   = dirichlet(TS_inf); 

int j;
int k_loop = 0;

int main() {
  periodic(top);

  L0 = 1.;
  CFL = 0.5;
  origin (-0.5*L0, -0.5*L0);
  // dist[top]    = neumann(-1.);  
  // dist[bottom] = neumann(-1.); 
  j = 1;
  for (j=1;j<=1;j++){

/**
Here we set up the parameters of our simulation. The latent heat $L_H$, the
initial position of the interface $h_0$ and the resolution of the grid.
*/
    latent_heat  = 1;
    MAXLEVEL = 7, MINLEVEL = 5 ;

    H0 = 0.5*L0; 
    N = 1 << MAXLEVEL;
    init_grid (N);
    run(); 
  }
}

event init(t=0){

  TOLERANCE = 1.e-7;
  DT        = 0.1*L0 / (1 << MAXLEVEL); // Delta
  // DT = 0.001;

  NB_width = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;
  foreach(){
      dist[] = clamp(plane(x,y,2),-1.1*NB_width,1.1*NB_width);
  }
  boundary ({dist});
  restriction({dist});

  redistance(dist);

  vertex scalar dist_n[];
  cell2node(dist,dist_n);

  fractions (dist_n, cs, fs);
  fractions_cleanup(cs,fs,smin = s_clean);
  boundary({cs,fs});
  restriction({cs,fs});

#ifdef CURVE_LS
    curvature_LS(dist,curve);
#else
    curvature(cs,curve);
#endif
  boundary({curve});

  // scalar curve2[];

  // curvature(cs,curve2);

  // scalar diff[];
  // foreach(){
  //   if(interfacial(point,cs)){
  //     diff[] = fabs(curve2[] - curve[])/fabs(curve[]);
  //   }
  //   else{
  //     diff[] = nodata;
  //   }
  // }
  // boundary({diff});
  // stats s2 = statsf(diff);
  // fprintf(stderr, "%g %g\n", s2.min, s2.max);
  // squares("diff");
  // save("diff.png");
  // squares("curve");
  // save("curve_LS.png");
  // squares("curve2");
  // save("curve_VOF.png");
  // dump();
  // exit(1);

  foreach() {
    TL[] = TL_inf ;
    TS[] = 0.;
  }

  foreach_face(){
    v_pc.x[] = 0.;
  }

  boundary({TL,TS});
  restriction({TL,TS});
}


event properties(i++){
  foreach_face()
  muv.x[] = lambda[i%2]*fs.x[];
  boundary((scalar *) {muv});
}

event tracer_diffusion(i++){
  int kk;
  mgstats mg1;
  for (kk=1;kk<=7;kk++){
    if(i%2==0){
      boundary({TL});
      mg1 = diffusion(TL, dt, D = muv , theta = cs);
    }
    else{
      boundary({TS});
      mg1 = diffusion(TS, dt, D = muv, theta = cs);
    }
    if((mg1.resa > TOLERANCE) ) {
/**
If the calculation crashes (it often does if the Poisson solver does not
converge) we save the last state of the variables
*/
      scalar ffsx[], ffsy[];
      scalar point_f[];
      vector x_interp[];
      foreach(){
        ffsx[] = fs.x[];
        ffsy[] = fs.y[];
        if(interfacial(point,cs)){
          coord n       = facet_normal( point, cs ,fs) , p;
          normalize(&n);
          double alpha  = plane_alpha (cs[], n);
          line_length_center (n, alpha, &p);
          x_interp.x[] = p.x;
          x_interp.y[] = p.y;
          coord segment[2];
          point_f[] = facets (n, alpha, segment);
        }
        else{
          x_interp.x[] = nodata;
          x_interp.y[] = nodata;
          point_f[]    = 0;
        }
      }
      boundary({ffsx,ffsy});
      vertex scalar dist_n[];
      cell2node(dist,dist_n);
      dump();
      fprintf(stderr, "%d\n", i);
      exit(1);
    }
  }
}


/**
This event is the core the of the hybrid level-set/embedded boundary.
*/
event LS_advection(i++,last){
  if(i%2 ==1){

    double L_H       = latent_heat;  

    scalar cs0[];

/**
We need to change cs to 1-cs for all the level-set functions to work.
*/
    foreach(){
      cs0[]   = cs[];
      cs[]    = 1.-cs[];
    }
    foreach_face(){
      fs.x[]  = 1.-fs.x[];
    }

    boundary({cs,fs,cs0});
    restriction({cs,fs});
/**
First, we calculate the velocity on the face centroid
*/
    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, L_H, 
      lambda,epsK, epsV, aniso);
    double deltat  = 0.45*L0 / (1 << MAXLEVEL);  // Delta
    int err = 0;
    int k_limit = 0;

/**
We copy this value in v_pc_r.
*/
    vector v_pc_r[];
    foreach(){
      foreach_dimension(){
        if(interfacial(point,cs))v_pc_r.x[] = v_pc.x[];
        else v_pc_r.x[] = 0.;
      }
    }
    boundary((scalar * ){v_pc_r});
    restriction((scalar * ){v_pc_r});

    scalar * speed_recons  = {v_pc_r.x,v_pc_r.y};

/**
We reconstruct a cell-centered v_pc field in the vicinity of the interface where
v_pc_r is the solution to the bilinear interpolation of v_pc on face centroids.
*/
    recons_speed(dist, deltat, nb_cell_NB, NB_width, speed_recons,
     k_limit, tolerance = 2.e-7, &err, nb_iter = 50, overshoot = 0.3);

    double dt_LS = timestep_LS (v_pc_r, DT);


/**
We do a cell-center to face change. Because we use embedded boundaries, we
cannot use the face_value routine.
*/
    face vector v_pc_f[];
    foreach_face(){
      v_pc_f.x[] = (v_pc_r.x[-1,0]+v_pc_r.x[])/2.;
    }
    boundary ((scalar *){v_pc_f});

    advection_LS (level_set, v_pc_f, dt_LS);
    
    boundary ({dist});
    restriction({dist});

/**
After the advection, we need to redistance the level-set function.
*/
    redistance(dist);

    scalar dist_n[];
    cell2node(dist,dist_n);
    fractions (dist_n, cs, fs);
    fractions_cleanup(cs,fs,smin = s_clean);

    boundary({cs,fs});
    restriction({cs,fs});

#ifdef CURVE_LS
    curvature_LS(dist,curve);
#else
    curvature(cs,curve);
#endif
    boundary({curve});

/**
We revert cs back to its original state.
*/
    foreach(){
      cs[]      = 1.-cs[];
    }
    foreach_face(){
      fs.x[]      = 1.-fs.x[];
    }
    boundary({cs,fs});
    restriction({cs,fs});

/**
Sometimes, when a new cell becomes an interfacial cell, the Poisson solver has
trouble converging, therefore we iterate a bit more when it occurs.
*/
    k_loop = 0;
    foreach(){
      if( (cs0[] != 1. && cs[] ==1.) || (cs0[] == 0. && cs[] !=0.))k_loop = 1;
    }
  }
}


#if DOUBLE_EMBED
event double_calculation(i++,last){
// modify the fs , cs, copy the outer fields in the partially covered cells

  foreach(){
    cs[]      = 1.-cs[];
  }
  foreach_face(){
    fs.x[]      = 1.-fs.x[];
  }

  boundary({cs,fs});
  restriction({cs,fs});
}
#endif

event movies ( i++,last;i < 150)
{
  if(i%2 == 1 ) {
    boundary({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});


    view (fov = 0.774205,  tx = -0.109326, ty = -0.210458);
    draw_vof("cs");
    squares("visu", min =-0.3, max = 0.1);
    save ("visu.mp4");

    draw_vof("cs");
    squares("curve");
    save ("curve.mp4");

    draw_vof("cs");
    squares("dist", min =-NB_width, max = NB_width);
    save ("dist.mp4");

      
    if(i%8==1) {
      output_facets (cs, stdout);
    }
  }
  // if(i%2 == 1){

  //   double y_max = -L0;
  //   stats s2 = statsf (v_pc.y);

  //   vector h[];
  //   heights (cs, h);
  //   boundary((scalar *){h});
  //   foreach(reduction(max:y_max)){
  //     if(interfacial(point, cs)){
  //       double yy = y+Delta*height(h.y[]);
  //       y_max = max(y_max,yy);
  //     }     
  //   }
  //   fprintf (stderr, "%.9f %.9f %.9f\n",
  //     t, s2.max, s2.min);
  // }
}

#if 1
event adapt (i++, last) {
  if(i%2 == 1 ){

    foreach_cell(){
      cs2[] = 1.-cs[];
    }
    foreach_face(){
        fs2.x[]      = 1.-fs.x[];
    }

    boundary({cs,cs2,fs,fs2});
    fractions_cleanup(cs,fs,smin   = s_clean);
    fractions_cleanup(cs2,fs2,smin = s_clean);
    restriction({cs,cs2,fs,fs2});
    int n=0;
    // stats s2 = statsf(curve);
    // fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
    boundary({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});

    adapt_wavelet ({cs,curve,visu},
      (double[]){1.e-2,1.e-2,1.e-2},MAXLEVEL, MINLEVEL);
    foreach(reduction(+:n)){
      n++;
    }
#ifdef CURVE_LS
    curvature_LS(dist,curve);
#else
    curvature(cs,curve);
#endif
    boundary({curve});

    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, latent_heat, 
      lambda,epsK, epsV, aniso);
    fprintf(stderr, "##nb cells %d %g\n", n, t);
  }
}
#endif



/**

![Animation of temperature field](mullins_sekerka/visu.mp4)(loop)

Here we plot the same figure Fig. 4 of [Chen et al.](#chen_simple_1997)
~~~gnuplot Evolution of the interface
set term @PNG enhanced size 1000,1000
set size ratio -1
set xrange [-0.5:0.5]
set yrange [-0.5:0.5]
plot 'out' w l t 'dt = 0.5*Delta'
~~~

~~~bib

@Article{Mullins1964,
  author        = {Mullins, William W and Sekerka, RF},
  title         = {Stability of a planar interface during solidification of a dilute binary alloy},
  journal       = {Journal of applied physics},
  year          = {1964},
  volume        = {35},
  number        = {2},
  pages         = {444--451},
  publisher     = {AIP},
}

@Article{Almgren1993,
  author        = {R. Almgren},
  title         = {Variational Algorithms and Pattern Formation in Dendritic Solidification},
  year          = {1993},
  volume        = {106},
  pages         = {337-354},
  issn          = {0021-9991},
  doi           = {10.1006/jcph.1993.1112},
}

@article{chen_simple_1997,
  title = {A simple level set method for solving {Stefan} problems},
  volume = {135},
  number = {1},
  journal = {Journal of Computational Physics},
  author = {Chen, S and Merriman, B and Osher, S and Smereka, P},
  year = {1997},
  pages = {8--29}
}
~~~
*/
