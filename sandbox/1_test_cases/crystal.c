/**
#Melt of a solid particle

We simulate the diffusion of two tracers separated by an embedded boundary. The
interface moves using the Stefan relation :

$$
  \mathbf{v}_{pc} = \frac{1}{L_H}(\lambda_1 \nabla T_L - \lambda_1 \nabla T_S)
  $$
where $L_H$ is the latent heat of water, $T_L$ and $T_S$ are the temperature
fields on both sides of the interface.

The boundaries of the domain are set at $T_L = 1$. The ice particle is initially
at $T_S = -1$.

The temperature on the interface is derived from the Gibbs-Thomson relation,
withtout any anisotropic effect :
$$
 T_{\Gamma} = T_{m} - \epsilon_{\kappa} * \kappa - \epsilon_{v} v_{pc}
$$
*/
#define DOUBLE_EMBED  1
#define LevelSet      1
#define Gibbs_Thomson 1

#include "embed.h"
#include "../double_embed-tree.h"

#include "../advection_A.h"
#include "diffusion.h"

#include "fractions.h"
#include "curvature.h"

#include "../level_set.h"
#include "../LS_reinit.h"
#include "../../popinet/redistance.h"
#include "../LS_recons.h"
#include "../LS_advection.h"
#include "../phase_change_velocity.h"
#include "../LS_curvature.h"
#include "view.h"

#define T_eq         0.
#define TL_inf       1.
#define TS_inf       -1.

/**
Setup of the numerical parameters
*/
int MAXLEVEL = 7; 
int MINLEVEL = 3;
double  H0;

/**
Setup of the physical parameters + level_set variables
*/
scalar TL[], TS[], dist[];
vector v_pc[];


scalar * tracers   = {TL};
scalar * tracers2  = {TS};
scalar * level_set = {dist};
scalar * LS_speed  = {v_pc.x,v_pc.y};
face vector muv[];
mgstats mgT;
scalar grad1[], grad2[];


double  latent_heat = 1.;
double  lambda[2];    // thermal capacity of each material
#if Gibbs_Thomson // parameters for the Gibbs-Thomson's equation
double  epsK = 0.005, epsV = 0.001;
#else
double  epsK = 0.000, epsV = 0.000;
#endif

#define GT_aniso 0
#if GT_aniso
int aniso = 6;
#else
int aniso = 1;
#endif

int     nb_cell_NB;
double  NB_width ;    // length of the NB

scalar curve[];

TL[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, v_pc, curve, fs, cs, aniso));
TS[embed] = dirichlet(T_eq + Temp_GT(point, epsK, epsV, v_pc, curve, fs, cs, aniso));

TL[top]    = dirichlet(TL_inf); 
TL[bottom] = dirichlet(TL_inf); 
TL[left]   = dirichlet(TL_inf); 
TL[right]  = dirichlet(TL_inf); 

TS[top]    = dirichlet(TS_inf); 
TS[bottom] = dirichlet(TS_inf); 
TS[left]   = dirichlet(TS_inf); 
TS[right]  = dirichlet(TS_inf); 

/**
Initial geometry definition. Here the interface equation is :

$$
r\left(1+ 0.2 *cos(8\theta) \right) - R
$$
where $r = \sqrt{x^2 + y^2}$, $\theta = \arctan(x,y)$ and $R = \frac{L_0}{5}$

Notice that the initial dist[] field is not really a distance, it is modified
after a few iterations of the LS_reinit() function.
*/
double geometry(double x, double y, double Radius) {

  coord center;
  center.x = 0.5;
  center.y = 0.5;

  double theta = atan2 (y-center.y, x-center.x);
  double R2  =  sq(x - center.x) + sq (y - center.y) ;
  double s = -( sqrt(R2)*(1.+0.25*cos(6*theta)) - Radius);
  // double s = -( sqrt(R2)*(1.+0.*cos(6*theta)) - Radius);


  return s;
}

/**
$k_{loop}$ is a variable used when the interface arrives in a new cell. Because
we are using embedded boundaries, the solver needs to converge a bit more when
the interface is moved to a new cell. Therefore, the Poisson solver is used
$40*k_{loop} +1$ times.
*/
int k_loop = 0;

int main() {

  TOLERANCE = 2.e-7;
  int N = 1 << MAXLEVEL;
  init_grid (N);
  run();
}


event init(t=0){
  DT         = 0.8*L0 / (1 << MAXLEVEL);  // Delta
  nb_cell_NB = 1 << 3 ;               // number of cell in the 
                                      // narrow band 
  NB_width   = nb_cell_NB * L0 / (1 << MAXLEVEL);

  lambda[0] = 1.;
  lambda[1] = 1.;
  foreach() {
    dist[] = clamp(-geometry(x,y,L0/3.),-1.5*NB_width,1.5*NB_width);
  }

  boundary ({dist});
  restriction({dist});


  redistance(dist);
  // LS_reinit(dist);


  vertex scalar dist_n[];
  int Stencil[2] = {-1,-1};
  coord p_interp = {-0.5, -0.5};
  foreach_vertex(){
    double coeff[4];
    dist_n[] = mybilin(point , dist, Stencil, p_interp, coeff);
  }

  boundary ({dist_n});
  restriction({dist_n});

  fractions (dist_n, cs, fs);

  foreach_face(){
    v_pc.x[] = 0.;
  }
  boundary((scalar *){v_pc});
  
  curvature(cs,curve);
  boundary({curve});
  // curvature_LS(dist, curve);
  foreach() {
    TL[] = TL_inf;
    TS[] = TS_inf;
  }
  boundary({TL,TS});
  restriction({TL,TS});
}

event properties(i++){
  foreach_face()
    muv.x[] = lambda[i%2]*fs.x[];
  boundary((scalar *) {muv});
}

event tracer_diffusion(i++){
  int kk;
  mgstats mg1;
  for (kk=1;kk<=3;kk++){ 
    if(i%2==0){
      boundary({TL});
      mg1 = diffusion(TL, dt, D = muv, theta = cs);
    }
    else{
      boundary({TS});
      mg1 = diffusion(TS, dt, D = muv, theta = cs);
    }
    if(mg1.resa > TOLERANCE) {
      scalar ffsx[], ffsy[];
      foreach(){
        ffsx[] = fs.x[];
        ffsy[] = fs.y[];
      }
      boundary({ffsx,ffsy});
      dump();
      exit(1);
    }
  }
}

event LS_advection(i++,last){
  if(i%2 ==1){

    scalar cs0[];

    foreach(){
      cs0[]   = cs[];
      cs[]    = 1.-cs[];
    }
    foreach_face(){
      fs.x[]  = 1.-fs.x[];
    }

    boundary({cs,fs,cs0});
    restriction({cs,fs});

    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, latent_heat, 
      lambda,epsK, epsV, aniso);

    double deltat  = 0.45*L0 / (1 << MAXLEVEL);  // Delta
    int err = 0;
    int k_limit = 0;

    vector v_pc_r[];
    foreach(){
      foreach_dimension(){
        if(interfacial(point, cs))v_pc_r.x[] = v_pc.x[];
        else v_pc_r.x[] = 0.;
      }
    }
    boundary((scalar * ){v_pc_r});
    restriction((scalar * ){v_pc_r});

    scalar * speed_recons  = {v_pc_r.x,v_pc_r.y};
    recons_speed(dist, deltat, nb_cell_NB, NB_width, speed_recons,
      k_limit, 2.e-7, &err, overshoot = 0.1);

    double dt_LS = timestep_LS (v_pc_r, DT);

    face vector v_pc_f[];
    foreach_face(){
      v_pc_f.x[] = (v_pc_r.x[-1,0]+v_pc_r.x[])/2.;
    }
    boundary ((scalar *){v_pc_f});

    advection_LS (level_set, v_pc_f, dt_LS);
    
    boundary ({dist});
    restriction({dist});

    // LS_reinit(dist);
    redistance(dist);

    vertex scalar dist_n[];
    int Stencil[2] = {-1,-1};
    coord p_interp = {-0.5, -0.5};
    foreach_vertex(){
      double coeff[4];

      dist_n[] = mybilin( point , dist, Stencil, p_interp, coeff);
    }
    boundary({dist_n});

    fractions (dist_n, cs, fs);
    
    boundary({cs,fs});
    restriction({cs,fs});

    foreach(){
      cs[]      = 1.-cs[];
    }
    foreach_face(){
      fs.x[]      = 1.-fs.x[];
    }

    boundary({cs,fs});
    restriction({cs,fs});

    k_loop = 0;
    foreach(){
      if( (cs0[] != 1. && cs[] ==1.) || (cs0[] == 0. && cs[] !=0.))k_loop = 1;
    }
  }

}

#if DOUBLE_EMBED
event double_calculation(i++,last){
// modify the fs , cs, copy the outer fields in the partially covered cells

  // scalar temp[];
  // temp = cs2;
  // cs2 = cs;
  // cs  = temp;

  // foreach_dimension(){
  //   temp = fs2.x;
  //   fs2.x = fs.x;
  //   fs.x  = temp;
  // }

  foreach(){
    cs[] = 1.-cs[];
  }
  foreach_face(){
    fs.x[] = 1.-fs.x[];
  }

  boundary({cs,fs});
  restriction({cs,fs});
}
#endif

event movies ( i++,last;t<2.4)
{
  if(i%12 == 1) {
    boundary({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    view (fov = 20.,  tx = -0.5, ty = -0.5, width = 800, height = 800);
    cells();
    draw_vof("cs");
    squares("visu",min = -0.1, max = 1);
    save ("visu.mp4");

  }
  if(i%50==1) {
    output_facets (cs, stdout);
  }
}

#if 1
event adapt (i++, last) {
  if(i%2 == 1 ){

    foreach_cell(){
      cs2[] = 1.-cs[];
    }
    foreach_face(){
        fs2.x[]      = 1.-fs.x[];
    }

    boundary({cs,cs2,fs,fs2});
    fractions_cleanup(cs,fs,smin = 1.e-14);
    fractions_cleanup(cs2,fs2,smin = 1.e-14);
    restriction({cs,cs2,fs,fs2});
    int n=0;
    stats s2 = statsf(curve);
    fprintf(stderr, "%g %g %g\n",t, s2.min, s2.max);
    boundary({TL,TS});
    scalar visu[];
    foreach(){
      visu[] = (cs[])*TL[]+(1.-cs[])*TS[] ;
    }
    boundary({visu});
    restriction({visu});

    adapt_wavelet ({cs,curve,visu},
      (double[]){1.e-3,1.e-3,1.e-2},MAXLEVEL, MINLEVEL);
    foreach(reduction(+:n)){
      n++;
    }
    curvature(cs,curve);
    boundary({curve});

    phase_change_velocity_LS_embed (cs, fs ,TL, TS, v_pc, latent_heat, 
      lambda,epsK, epsV, aniso);
    fprintf(stderr, "##nb cells %d\n", n);
  }
}
#endif

/**
![Animation of the approximated temperature field](crystal/visu.mp4)(loop)

~~~gnuplot Temperature on the boundary
set key left
plot 'log' u 1:2 w l t 'curvature min',  \
     'log' u 1:3 w l  t 'curvature max'
~~~

~~~gnuplot Evolution of the interface (zoom)
set term @PNG enhanced size 1000,1000
set size ratio -1
set key top right
unset xlabel
unset xtics
unset ytics
unset border
unset margin
unset ylabel
plot 'out' w l lw 2
~~~
*/